const redis = require('redis');
var containerized = require('containerized');

// defaults to localhost
var host = 'localhost';

// if running in container user link name
if (containerized()) {
	host = 'redis';
}


const client = redis.createClient(6379, host, {
  retry_strategy: function (options) {
      // reconnect after
      return Math.min(options.attempt * 100, 3000);
  }
});

// gets current status of the item.
const getStatus = id => new Promise((resolve, reject) => {
  client.get(id, (err, reply) => {
    // if we don't find a match we return null status
    if (err) {
      console.log(`[x] ${err}`);
      reject(500);
    } else {
      
      if (reply) {
        // if we find a match we create a json string for it and return it
        console.log(` [v] Got ${id} status: '${reply.toString()}'`);
        resolve(reply.toString());
      } else {
        reject(404);
      }
    }
  });
});

// updates the status of the item on Redis
const setStatus = (id, status) => new Promise((resolve, reject) => {
  client.set(id, status, (err, reply) => {
    if (err) {
      reject(err);
    } else {
      console.log(` [v] Set ${id} status: '${status}'`);
      resolve(reply);
    }
  });
});

// enerates a unique id to store the item on Redis.
const generateId = key => new Promise((resolve, reject) => {
  client.incr(key, (err, id) => {
    if (err) {
      reject(err);
    } else {
      resolve(id);
    }
  });
});


module.exports = {
  getStatus,
  setStatus,
  generateId,
};
