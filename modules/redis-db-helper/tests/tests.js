const assert = require('assert');
const db = require('../app');

// tries to set a value for a key on Redis and fetch it again to make sure it's correct
it('Set and Get value from Redis', (done) => {
  assert.doesNotThrow(() => {
    db.setStatus('testKey', 'testStatus').then((reply) => {
      if (reply !== 'OK') {
        throw new Error('Redis value not saved correctly as reply is not OK');
      }
      // if a status is set, we try to fetch it to check it
      return db.getStatus('testKey');
    }, (error) => {
      throw new Error(error);
    }).then((value) => {
      assert.equal(value, 'testStatus');
      done();
    }, (error) => {
      throw new Error(error);
    }).catch((error) => {
      done(error);
    });
  });
});

// checks if a unique id can be generated on Redis by incremented by one the 'id' key.
// This is unique between clients as variable is locked while it gets incremented
it('Generate unique id on Redis', (done) => {
  assert.doesNotThrow(() => {
    // sets the key to 0
    db.setStatus('testId', 0).then((reply) => {
      if (reply !== 'OK') {
        throw new Error('Redis value not saved correctly as reply is not OK');
      }
      return db.generateId('testId');
    }, (error) => {
      throw new Error(error);
    }).then((value) => {
      const expectedValue = 1;
      // checks if generated id is equal to previous id + 1
      assert.equal(value, expectedValue);
      done();
    }, (error) => {
      throw new Error(error);
    }).catch((error) => {
      done(error);
    });
  });
});
