var containerized = require('containerized');

// defaults to localhost
var host = 'amqp://127.0.0.1';

// if running in container user link name
if (containerized()) {
	host = 'amqp://rabbitmq';
}

var open = require('amqplib').connect(host);

// this is a default queue name incase one is not set
var queue = 'default-queue';

// initialize with a queue name and return a promise to a valid channel
var init = (name) => new Promise((resolve, reject) =>  {
	// update the current queue name 
	queue = name

	// open connection to rabbitmq
	open.then( (conn) => {
		return conn.createChannel();
	}, (error) => {
		reject(error);
	}).then((ch) => {
		return ch.assertQueue(queue, {durable: false}).then(function(ok) {
			resolve(ch);
		});
	}, (error) => {
		reject(error);
	}).catch(console.warn);
	
})

// pushes job to current queue via the passed channel
var addToQueue = ( job , channel)  => {
	
	return channel.sendToQueue(queue, new Buffer(job));
}

module.exports = {
	init,
	addToQueue
}

