const assert = require('assert');
const queueHelper = require('../app');

// tries to set a value for a key on Redis and fetch it again to make sure it's correct
it('Add task to queue', (done) => {
  assert.doesNotThrow(() => {
    queueHelper.init('test_queue').then(channel => queueHelper.addToQueue('testJob', channel),
      (error) => {
        throw new Error(error);
      }).then((result) => {
      assert.equal(result, true);
      done();
    }, (error) => {
      throw new Error(error);
    }).catch((error) => {
      done(error);
    });
  });
});
