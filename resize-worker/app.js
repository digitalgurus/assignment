const sharp = require('sharp');

// imports database helper
const db = require('redis-db');
const queueHelper = require('queue-json-helper');

// defines location of media folder
const fileStorage = '../media';

// resizes the image based on the msg payload and returns a promise. Slow processing is simulating by adding a timeout of x ms
const resizeImage = job => new Promise((resolve, reject) => {
    
  sharp(`${fileStorage}/${job.id}${job.fileExtension}`)
    .resize(100, 100)
    .toFile(`${fileStorage}/resized/${job.id}.png`, (err) => {
      if (err) {
        console.log(' [x]', err);
        reject(err);
      } else {
        // simulate long job
        setTimeout(() => {
          db.setStatus(job.id, 'resized').then(() => {
            resolve('resized');
          }, (error) => {
            reject(error);
          });
        }, 1000);
      }
    });
});

// initialises the helper with a queue name
queueHelper.init('resize-queue').then((channel) => {
channel.prefetch(1);
console.log(' [*] Waiting for messages in. To exit press CTRL+C');
channel.consume('resize-queue', (msg) => {
	if (msg !== null) {
		console.log(` [v] Fetched ${msg.content.toString()}`);
		const job = JSON.parse(msg.content.toString());
		resizeImage(job).then(() => {
			channel.ack(msg);
		}, (error) => {
			console.log(` [x] ${error}`);
			channel.reject(msg);
		}).catch(console.warn);
	} else {
		channel.reject(msg);
	}
}, { noAck: false });
});


module.exports = {
	resizeImage,
	fileStorage
};