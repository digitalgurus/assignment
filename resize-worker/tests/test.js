const assert = require('assert');

it('should resize an image', (done) => {
  var fs = require("fs")

  function checkFileExists(filepath){
    return new Promise((resolve, reject) => {
      fs.access(filepath, fs.F_OK, error => {
        resolve(!error);
      });
    });
  }

  assert.doesNotThrow(() => {
    var app = require('../app');
    var msg = {"id":1,"fileExtension":".jpg"};
    
    app.resizeImage(msg).then(() => {
      checkFileExists("../media/resized/1.png").then( (result) => {
        assert.equal(result, true)
        done();
      }, (error) => {
        throw new Error(` [x] ${error}`);
      }).catch((error) => {        
        done(error);
      });
    }, (error) => {
      throw new Error(` [x] ${error}`);
    }).catch((error) =>{
      done(error);
    });
  });
})
