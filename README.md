# Backend Application Challenge

This repository contains a NodeJS implementation of a system where users can:  
- post images to an HTTP endpoint (POST /image)  
- images posted will be resized to a 100x100px resolution  
- users can retrieve the resized image later on, once the resize job is completed by accessing an HTTP endpoint (GET /image/${imageId}/thumbnail)     

This implementation has the following characteristics:  
- a Datastore Helper implemented as an independent local module, using Redis as a simple key-value store . Redis can be easily swapped for any other datastore solution without requiring any change to the rest of the system. This helps with code reusability, testability and maintainability.   
- a Queue Helper implemented as an independent local module, using RabbitMQ.  
- a NodeJS/Express Web Server app, that receives requests from the user via a simple API and responds with JSON messages or http error codes.  
- one or multiple Resize Worker apps, that subscribe to the work queue and continually process incoming messages. A 1000ms delay has been added on purpose to the resizeImage function to simulate a long running job.  
- a file storage as a shared Docker Volume, accessible at ./media

### Prerequisites

To deploy the system you will need the following tools:  
- docker CE (https://docs.docker.com/install/linux/docker-ce/binaries/)  
- docker Compose (https://docs.docker.com/compose/install/)  
- Git (https://git-scm.com/)  

### Installing

git clone https://sergio_cecarini@bitbucket.org/digitalgurus/assignment.git  
cd assignment  
docker-compose up  

It's possible to deploy an higher number of Resize Workers, to simulate horizontal scaling of the system with this command:  
docker-compose up --scale worker=20

The API will be available on port 3000 and these are Curl usage examples of the two available endpoints:

POST /image  
- curl -F image=@/path/test.png  127.0.0.1:3000/image  
- the system will return a JSON response in this format: {"id":1,"status":"processing"}

GET /image/${imageId}/thumbnail  
- curl 127.0.0.1:3000/image/1/thumbnail  
- the system will return a JSON response in this format: {"id":1,"status":"processing"} when the job is still in the queue or {"id":"1","status":"resized","thumbnail":"http://localhost:3000/media/resized/2.png"} , when resize job is completed.

To stress test the system something like this can be used:  
- while true; do curl -F image=@/path/test.png  127.0.0.1:3000/image; sleep 0.1; done



## Automated Tests

Each one of the four modules contains its own test cases (tests/test.js), but for demo purposes you can run all the test suites sequentially with this command:

docker-compose -f docker-compose.test.yml up

This will test the following modules:  
- queue helper  
- database helper  
- web server   
- resize worker  

## Production Deployment

The system's containers can be easily orchestrated and deployed to a Rancher swarm on Amazon Ec2, to  Amazon Elastic Container or to Google Kubernetes. The system can also be setup to scale up and down the number of Resize Workers based on live workload and/or queue length.
I would recommend to set up a Jenkins pipeline for CI/CD, that automatically triggers integration tests from a git push and consequently deployes the system to one of the enviroments mentioned previously. 

## TODO
- Implementation of DB persistence so that jobs are not lost on a database restart.
- Improved handling of RabbitMQ and Redis unexpected restarts
- Scale number of Resize Workers up and down automatically based queue length and workload.
- Add better api error responses
- Write more exhaustive tests
- Add authentication to Redis / RabbitMQ connections
- Add api authentication
- Add load balancer to the stack to dynamically scale up and down number of web servers

## Built With

* [Docker](https://www.docker.com/)
* [Node.js](https://nodejs.org/) 
* [Redis](https://redis.io/) 
* [RabbitMQ](https://www.rabbitmq.com/) 
* [Visual Studio Code](https://code.visualstudio.com/) 

## Authors

* **Sergio Cecarini** -

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details