const express = require('express');
const fileUpload = require('express-fileupload');
const path = require('path');

// imports local modules
const db = require('redis-db');
const queueHelper = require('queue-json-helper');

// port and host for the web service
const PORT = 3000;
const fileServer = '../media';
const app = express();

let channel = '';
// initializes queue helper
queueHelper.init('resize-queue').then((ch) => {
  channel = ch;
}, (error) => {
  console.log(error);
}).catch(console.warn);

// uses express-fileupload to handle the image file post
app.use(fileUpload());
app.use('/media', express.static(fileServer));

// defines route to retrieve image thumbnail
app.get('/image/:id/thumbnail', (req, res) => {
  const jobId = req.params.id;
  db.getStatus(jobId).then((status) => {
      var response = {'id':jobId, 'status':status}
      if (status == 'resized'){
        response.thumbnail = `http://localhost:${PORT}/media/resized/${jobId}.png`;
      }
      res.json(response);
  }, (error) => {    
    res.sendStatus(error);
  }).catch(console.warn);
});

// defines route to post new image
app.post('/image', (req, res) => {

  if (!req.files) {
    res.sendStatus(400);
  }
  
  // we use the image field to retrieve the uploaded file.
  const imageData = req.files.image;
  // grab file name and extension  
  const fileName = imageData.name;
  const fileExtension = path.extname(imageData.name);
  console.log(' [v] Received', fileName);

  let finalId = 0;
  let finalJob = '';

  // generate unique id via Redis, so that it's unique between instances
  db.generateId('id')
    .then((id) => {
      finalId = id;
      return imageData.mv(`${fileServer}/${id}${fileExtension}`);
    },
    (error) => {
      console.log(' [x] error generating id');
      res.sendStatus(500);
    })
    .then(() => {
      console.log(` [v] Saved ${fileName}  to File storage`);
      return db.setStatus(finalId, 'processing');
    }, (error) => {
      console.log(' [x] error moving file to file storage');
      res.sendStatus(500);
    })
    .then(() => {
    // create job payload
      const job = JSON.stringify({ id: finalId, fileExtension });
      finalJob = job;

      // add resize process to the queue
      return queueHelper.addToQueue(job, channel);
    }, (error) => {
      console.log(' [x] error changing status');
      res.sendStatus(500);
    })
    .then(() => {
      console.log(` [v] Sent ${finalJob}`);
      // return id to the user
      res.json({'id':finalId, 'status':'processing'});
    }, (error) => {
      console.log(' [x] error adding to queue');
      res.sendStatus(500);
    });
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}!`);
});

// set exports for unit testing
module.exports = app;