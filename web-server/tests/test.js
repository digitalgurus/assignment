var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../');
var should = chai.should();

chai.use(chaiHttp);

it('should POST an image and return id', (done) => {
  chai.request(server)
    .post('/image')
    .attach('image', 'tests/jimi_bitmap_full.jpg', 'jimi_bitmap_full.jpg')
    .end((err, res) => {        
        if (err) {
            console.log(err)
        } else {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('id');
            res.body.should.have.property('status').eql('processing');
        }
        done();
    })
})

it('should GET thumbnails with random id and return 404', function(done) {
  chai.request(server)
    .get('/image/RANDOM/thumbnail')
    .end(function(err, res){
      res.should.have.status(404);
      done();
    });
});

it('should GET random endpoint and return 404', function(done) {
  chai.request(server)
    .get('/imadddgembddnail')
    .end(function(err, res){
      res.should.have.status(404);
      done();
    });
});

it('should GET an exisiting thumbnail and return status', function(done) {  
  chai.request(server)
    .get(`/image/1/thumbnail`)
    .end(function(err, res){
      res.should.have.status(200);
      done();
    });
});